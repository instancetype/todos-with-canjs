/**
 * Created by instancetype on 9/4/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
;(function() {

  const TODOS =
    [ { id: 1, name: 'Feed the cats' }
    , { id: 2, name: 'Wash dishes' }
    , { id: 3, name: 'Buy printer ink' }
    ]

  can.fixture('GET /todos', function() {
    return TODOS
  })

  can.fixture('GET /todos/{id}', function(request) {
    return TODOS[(+request.data.id) - 1]
  })

  can.fixture('POST /todos', function(request) {
    var id = TODOS.length + 1
    TODOS.push( $.extend({ id : id }, request.data))

    return { id : id }
  })

  can.fixture('PUT /todos/{id}', function(request) {
    $.extend( TODOS[(+request.data.id) -1], request.data )
    return {}
  })

  can.fixture('DELETE /todos/{id}', function() {
    return {}
  })

})()



const Todo = can.Model(
  { findAll : 'GET /todos'
  , findOne : 'GET /todos/{id}'
  , create  : 'POST /todos'
  , update  : 'PUT /todos/{id}'
  , destroy : 'DELETE /todos/{id}'
  }
, {}
)

var
  Todos = can.Control({
    defaults: { view: 'todos.ejs' }}

  , { init : function(element, options) {
        Todo.findAll({}, function(todos) {
          element.html(can.view(options.view, todos))
        })
      }
    , 'li click' : function(li, event) {
        var todo = li.data('todo')
        li.trigger('selected', todo)
      }
    , 'li .destroy {Events.destroy}' : function(el, event) {
        var todo = el.closest('li').data('todo')
        todo.destroy()
        event.stopPropagation()
      }
    }
  )
var Events = { destroy : 'click' }


var
  Editor = can.Control({
    todo : function(todo) {
      this.options.todo = todo
      this.on()
      this.setName()
      this.element.show()
    }
  , setName : function() {
      this.element.val( this.options.todo.name )
    }
  , 'change' : function() {
      var todo = this.options.todo
      todo
        .attr( 'name', this.element.val() )
        .save()
    }
  , '{todo} destroyed' : function() {
      this.element.hide()
    }
  })


var
  Routing = can.Control({
      init : function() {
        this.editor = new Editor('#editor')
        new Todos('#todos')
        can.route.ready()
      }
    , 'route' : function() {
        this.editor.element.hide()
      }
    , 'todos/:id route' : function(data) {
        var editor = this.editor
        Todo.findOne(data, function(todo) {
          editor.todo(todo)
        })
      }
    , 'li selected' : function(el, ev, todo) {
        can.route.attr('id', todo.id)
      }
    }
  )

new Routing(document.body)
